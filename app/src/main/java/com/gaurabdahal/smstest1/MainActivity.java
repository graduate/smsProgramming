package com.gaurabdahal.smstest1;


import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button myButton,myButton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myButton = (Button) findViewById(R.id.btn1);
        myButton.setOnClickListener(this);

        myButton2 = (Button) findViewById(R.id.btn2);
        myButton2.setOnClickListener(this);
    }
    @Override
    public void onClick (View v) {
        switch(v.getId()){
            case R.id.btn1:
                Send_SMS();
                break;
            case R.id.btn2:
                Start_SMS_App();
                break;
        }

        //Start_SMS_App();
    }
    private void Send_SMS() {
        String phoneNo = "4096004573";
        String message = "Hello, how are you today?";

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(getApplicationContext(), "SMS sent", Toast.LENGTH_LONG).show();
        }
        catch (Exception e) {
            Toast.makeText(getApplicationContext(), "SMS failed", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    private void Start_SMS_App() {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);

        smsIntent.setData(Uri.parse("smsto:"));
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", new String("4096004573"));
        smsIntent.putExtra("sms_body", "Test message 2");

        try {
            startActivity(smsIntent);
            finish();
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "SMS failed", Toast.LENGTH_SHORT).show();
        }
    }
}
